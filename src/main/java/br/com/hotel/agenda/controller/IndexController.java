package br.com.hotel.agenda.controller;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexController {

	@GetMapping("/")
	public ModelAndView inicio(Map<String, Object> model,  HttpSession session) {

		 ModelAndView mv =  new ModelAndView("index", "obj", new Object()) ;
		 
		 return mv;
	}
}
